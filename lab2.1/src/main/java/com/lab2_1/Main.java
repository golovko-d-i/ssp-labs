package com.lab2_1;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Lab 3. Description:
 * <p>
 *  Протабулировать функции
 *  f1(x) = lnx if(0 < x ≤ 0.5)
 *  f2(x) = cos(x) + z if(0.5 < x ≤ 1)
 *  f3(x) = x2 if(1 < x < 2)
 *  где x ∈ (0..2), hx = 0.1, z = {−1,1}.
 *
 *   Обрамить значение максимума в <...>, а значение минимума в >...<.
 *
 *   US, Ширина 3, Точность 2, Выравнивание по левому краю
 */
public final class Main {

    private static final float STEP = 0.1f;
    private static final float START_X = 0 + STEP;
    private static final float END_X = 2;
    private static final int[] Z = new int[]{-1, 1};

    private static final Locale LOCALE = Locale.US;
    private static final String LOCALE_FORMAT = "%3.2f";
    private static final String HEADER =   "|Номер итерации|Значение xi|Значение f1|Значение f2|Значение f3|";
    private static final String FUN_TEMPLATE = "|%-11.11s|%-11.11s|%-11.11s|";

    private static double min = 0;
    private static double max = 0;

    /**
     * @param args program input arguments
     */
    public static void main(String[] args) {

        try {
            ArrayList<Double> values = new ArrayList<>();
            ArrayList<Double> xs = new ArrayList<>();
            ArrayList<Integer> poss = new ArrayList<>();

            int i = 1;
            double x = START_X;

            // calculation
            while (Double.compare(x, END_X) < 0) {

                if (Double.compare(x, 0.5f) <= 0) {
                    values.add(Math.log(x));
                    xs.add(x);
                    poss.add(i);
                } else if (x > 1) {
                    values.add(Math.pow(x, 2));
                    xs.add(x);
                    poss.add(i);
                } else {
                    for (int next : Z) {
                        poss.add(i);
                        xs.add(x);
                        values.add(Math.cos(x) + next);
                    }

                }

                min = Math.min(min, values.get(values.size() - 1));
                max = Math.max(max, values.get(values.size() - 1));

                i++;
                x += STEP;
            }

            System.setOut(new PrintStream(new FileOutputStream("output2_1.txt")));
            System.out.println(HEADER);

            for (i = 0; i < values.size(); i++) {
                double value = values.get(i);
                x = xs.get(i);

                StringBuilder builder = new StringBuilder();
                builder.append(String.format("|%-14s|%-11s", poss.get(i), String.format(LOCALE, LOCALE_FORMAT, x)));

                if (Double.compare(x, 0.5f) <= 0) {
                    System.out.println(builder.append(String.format(FUN_TEMPLATE, getFormattedValue(value), "", "")));
                } else if (Double.compare(x, 1f) > 0) {
                    System.out.println(builder.append(String.format(FUN_TEMPLATE, "", "", getFormattedValue(value))));
                } else {
                    System.out.println(builder.append(String.format(FUN_TEMPLATE, "", getFormattedValue(value), "")));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getFormattedValue(double value) {
        String format;
        if (Double.compare(value, min) == 0) {
            format = ">" + LOCALE_FORMAT + "<";
        } else if (Double.compare(value, max) == 0) {
            format = "<" + LOCALE_FORMAT + ">";
        } else {
            format = LOCALE_FORMAT;
        }
        return String.format(LOCALE, format, value);
    }
}