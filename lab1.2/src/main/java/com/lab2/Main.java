package com.lab2;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Lab 2. Description:
 * <p>
 * Вметоде вставок на j-ом шаге j-ый элемент вставляется в M[j] внужную позицию среди элементов M[1], M[2],..., M[j-1],
 * которые уже упорядочены. После этой вставки первые j элементов массива M будут упорядочены
 */
public final class Main {

    /**
     * @param args program input arguments
     */
    public static void main(String[] args) {

        Scanner scannerIn = null;
        try {

            scannerIn = new Scanner(System.in);

            ArrayList<Integer> mas = new ArrayList<>();
            while (scannerIn.hasNextInt()) {
                mas.add(scannerIn.nextInt());
            }

            Integer[] data = new Integer[mas.size()];
            data = mas.toArray(data);

            for (int i = 0; i < data.length; i++) {

                int val = data[i];
                int j = i - 1;

                while (j >= 0 && data[j] > val) {
                    data[j + 1] = data[j];
                    j--;
                }
                data[j + 1] = val;
            }

            System.setOut(new PrintStream(new FileOutputStream("output1_2.txt")));
            System.out.printf("Result array: %s", Arrays.toString(data));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (scannerIn != null) {
                scannerIn.close();
            }
        }

    }
}