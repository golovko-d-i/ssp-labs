package com.lab;

import java.util.Scanner;

/**
 * Lab 1. Description:
 * <p>
 * Вывод моды последовательности. Мода последовательности — это наиболее часто встречаемое число в последовательности.
 * Например, если последовательность имеет вид 123445467, то её модой является число 4, которое встречается в
 * последовательности три раза.
 */
public final class Main {

    private static final int MAX_VALUE = 9;
    private static final int MIN_VALUE = 0;

    /**
     * @param args program input arguments
     */
    public static void main(String[] args) {

        Scanner scanner = null;
        try {

            scanner = new Scanner(System.in);

            int[] data = new int[10];

            while (scanner.hasNextInt()) {
                int dig = scanner.nextInt();

                if (dig > MAX_VALUE || dig < MIN_VALUE) {
                    System.out.printf("Values must be <= %d and >= %d (value = %d)\n", MAX_VALUE, MIN_VALUE, dig);
                } else {
                    data[dig] = ++data[dig];
                }
            }

            int maxIndex = 0;
            for (int i = 1; i < data.length; i++) {
                int newNumber = data[i];
                if ((newNumber > data[maxIndex])) {
                    maxIndex = i;
                }
            }

            System.out.printf("Mode is %d", maxIndex);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

    }
}